# Critters

A world of autonomous critters.

## To Run
`npm start`

## Deploy
Before deploy run `browserify app.js -o main.js`

## Code

This code is an adaptation of the code found in [this tutorial](http://html5hub.com/build-a-javascript-particle-system/).
