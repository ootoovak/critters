var WorldView = require('../views/worldView')
var World     = require('../models/world')

var WorldController = function() {
  this.model = new World()
  this.view  = new WorldView(this.model)
}

WorldController.prototype = {
  start: function() {
    this.initialize()
    this.view.cycle()
  },
  initialize: function() {
    this.model.create()
  },
}

module.exports = WorldController
