var Screen = function() {
  this.canvas = document.querySelector('#world')
  this.ctx    = this.canvas.getContext('2d')
  this.width  = this.canvas.width  = window.innerWidth
  this.height = this.canvas.height = window.innerHeight
}

Screen.prototype = {
  clear: function() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
  },
  drawSquare: function(args) {
    var px = args.point.x * this.width
    var py = args.point.y * this.height
    this.ctx.fillStyle = args.colour
    this.ctx.fillRect(px, py, args.size, args.size)
  },
}

module.exports = Screen
