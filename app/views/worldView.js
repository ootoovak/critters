var Screen   = require('./screen')
var Plant    = require('../models/plant')
var Prey     = require('../models/prey')
var Predator = require('../models/predator')

var WorldView = function(world) {
  this.world  = world
  this.screen = new Screen()
}

WorldView.prototype = {
  cycle: function() {
    this.clear()
    this.update()
    this.draw()
    this.queue()
  },
  clear: function() {
    this.screen.clear()
  },
  update: function() {
    this.world.update()
  },
  draw: function() {
    this.drawBeings()
  },
  queue: function() {
    window.requestAnimationFrame(this.cycle.bind(this))
  },
  drawBeings: function() {
    this.world.beings.forEach(this.drawBeing.bind(this))
  },
  drawBeing: function(being) {
    this.screen.drawSquare({
      size   : 3,
      point  : being.position,
      colour : this.beingColour(being)
    })
  },
  beingColour: function(being) {
    if(being instanceof Plant) {
      return 'rgba(0, 255, 0, 0.9)'
    }
    else if(being instanceof Prey) {
      return 'rgba(255, 255, 0, 0.9)'
    }
    else if(being instanceof Predator) {
      return 'rgba(255, 0, 0, 0.9)'
    }
  },
}

module.exports = WorldView
