var Predator = function(position) {
  this.position = position
}

Predator.prototype = {
  type: "Predator",
  move: function(close) {
    if(close.length > 0) {
      this.adjust(close)
    }
    else {
      this.wander()
    }
  },
  wander: function() {
    this.position.random()
  },
  adjust: function(close) {
    close.forEach(function(being) {
      var position = being.position
      if(being.type == "Predator") {
        this.position.towards(position, 0.1)
      }
      else if(being.type == "Prey") {
        this.position.towards(position, 0.25)
      }
    }.bind(this))
  },
}

module.exports = Predator
