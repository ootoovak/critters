var Point = function(x, y) {
  this.x = x
  this.y = y
}

Point.prototype = {
  clone: function() {
    return new Point(this.x, this.y)
  },
  opposite: function(point) {
    return point.y - this.y
  },
  adjacent: function(point) {
    return point.x - this.x
  },
  hypotenuse: function(point) {
    var forY = Math.pow(this.opposite(point), 2)
    var forX = Math.pow(this.adjacent(point), 2)
    return Math.sqrt(forX + forY)
  },
  distance: function(point) {
    return this.hypotenuse(point)
  },
  away: function(point, amount) {
    var y = this.opposite(point)
    var x = this.adjacent(point)
    var h = this.hypotenuse(point)
    var angle = Math.atan(y/x)
    var dy = Math.sin(angle) * h / 1000
    var dx = Math.cos(angle) * h / 1000
    this.moveY(dy)
    this.moveX(dx)
  },
  towards: function(point, amount) {
    var y = this.opposite(point)
    var x = this.adjacent(point)
    var h = this.hypotenuse(point)
    var angle = Math.atan(y/x) * Math.PI
    var dy = Math.sin(angle) * h / 1000
    var dx = Math.cos(angle) * h / 1000
    this.moveY(dy)
    this.moveX(dx)
  },
  random: function(max, min) {
    var max = typeof max !== 'undefined' ? max : 0.001
    var min = typeof max !== 'undefined' ? max : 0.0001
    var diry = Math.random() < 0.5 ? -1 : 1
    var dirx = Math.random() < 0.5 ? -1 : 1
    var y = Math.random() * (max - min) + min
    var x = Math.random() * (max - min) + min
    this.moveY(y * diry)
    this.moveX(x * dirx)
  },
  moveY: function(byY) {
    var newY = this.y + byY
    if(0 > newY || newY < 1) {
      this.y += byY
    }
  },
  moveX: function(byX) {
    var newX = this.x + byX
    if(0 > newX || newX < 1) {
      this.x += byX
    }
  },
}

module.exports = Point
