var Point    = require('./point')
var Plant    = require('./plant')
var Prey     = require('./prey')
var Predator = require('./predator')

var World = function() {
  this.beings = []
}

World.prototype = {
  numberOfPlants:     100,
  numberOfPrey:       750,
  numberOfPredators:  25,
  create: function() {
    this.generatePlants()
    this.generatePrey()
    this.generatePredators()
  },
  generatePlants: function() {
    var start = new Point(0.5, 0.55)
    this.generateBeings(start, this.numberOfPlants, Plant)
  },
  generatePrey: function() {
    var start = new Point(0.5, 0.5)
    this.generateBeings(start, this.numberOfPrey, Prey)
  },
  generatePredators: function() {
    var start = new Point(0.5, 0.45)
    this.generateBeings(start, this.numberOfPredators, Predator)
  },
  generateBeings: function(start, count, type) {
    for(var i = 0; i < count; i++) {
      this.generateBeing(start, type)
    }
  },
  generateBeing: function(start, type) {
    var randomStart = start.clone()
    randomStart.random(0.05, 0.005)
    var being = new type(randomStart)
    this.beings.push(being)
  },
  update: function() {
    this.beings.forEach(this.move.bind(this))
  },
  move: function(being) {
    var close = this.closeBeings(being)
    being.move(close)
  },
  closeBeings: function(currentBeing) {
    var maxDistance = 0.1
    var close = []
    this.beings.forEach(function(being) {
      if(being !== currentBeing) {
        var distance = currentBeing.position.distance(being.position)
        if(distance < maxDistance) { close.push(being) }
      }
    })
    return close;
  },
}

module.exports = World
