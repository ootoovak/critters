var Plant = function(position) {
  this.position = position
}

Plant.prototype = {
  type: "Plant",
  move: function(close) {
    this.wander()
  },
  wander: function() {
    this.position.random()
  }
}

module.exports = Plant
