var Prey = function(position) {
  this.position = position
}

Prey.prototype = {
  type: "Prey",
  move: function(close) {
    if(close.length > 0) {
      this.adjust(close)
    }
    else {
      this.wander()
    }
  },
  wander: function() {
    this.position.random()
  },
  adjust: function(close) {
    close.forEach(function(being) {
      var position = being.position
      if(being.type == "Predator") {
        this.position.away(position, 0.3)
      }
      else if(being.type == "Prey") {
        this.position.towards(position, 0.1)
      }
      else if(being.type == "Plant") {
        this.position.towards(position, 0.5)
      }
    }.bind(this))
  },
}

module.exports = Prey
