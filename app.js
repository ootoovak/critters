var WorldController = require('./app/controllers/worldController')

document.addEventListener('DOMContentLoaded', function() {
  var app = new WorldController()
  app.start()
})
