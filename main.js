(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
var WorldController = require('./app/controllers/worldController')

document.addEventListener('DOMContentLoaded', function() {
  var app = new WorldController()
  app.start()
})

},{"./app/controllers/worldController":2}],2:[function(require,module,exports){
var WorldView = require('../views/worldView')
var World     = require('../models/world')

var WorldController = function() {
  this.model = new World()
  this.view  = new WorldView(this.model)
}

WorldController.prototype = {
  start: function() {
    this.initialize()
    this.view.cycle()
  },
  initialize: function() {
    this.model.create()
  },
}

module.exports = WorldController

},{"../models/world":7,"../views/worldView":9}],3:[function(require,module,exports){
var Plant = function(position) {
  this.position = position
}

Plant.prototype = {
  type: "Plant",
  move: function(close) {
    this.wander()
  },
  wander: function() {
    this.position.random()
  }
}

module.exports = Plant

},{}],4:[function(require,module,exports){
var Point = function(x, y) {
  this.x = x
  this.y = y
}

Point.prototype = {
  clone: function() {
    return new Point(this.x, this.y)
  },
  opposite: function(point) {
    return point.y - this.y
  },
  adjacent: function(point) {
    return point.x - this.x
  },
  hypotenuse: function(point) {
    var forY = Math.pow(this.opposite(point), 2)
    var forX = Math.pow(this.adjacent(point), 2)
    return Math.sqrt(forX + forY)
  },
  distance: function(point) {
    return this.hypotenuse(point)
  },
  away: function(point, amount) {
    var y = this.opposite(point)
    var x = this.adjacent(point)
    var h = this.hypotenuse(point)
    var angle = Math.atan(y/x)
    var dy = Math.sin(angle) * h / 1000
    var dx = Math.cos(angle) * h / 1000
    this.moveY(dy)
    this.moveX(dx)
  },
  towards: function(point, amount) {
    var y = this.opposite(point)
    var x = this.adjacent(point)
    var h = this.hypotenuse(point)
    var angle = Math.atan(y/x) * Math.PI
    var dy = Math.sin(angle) * h / 1000
    var dx = Math.cos(angle) * h / 1000
    this.moveY(dy)
    this.moveX(dx)
  },
  random: function(max, min) {
    var max = typeof max !== 'undefined' ? max : 0.001
    var min = typeof max !== 'undefined' ? max : 0.0001
    var diry = Math.random() < 0.5 ? -1 : 1
    var dirx = Math.random() < 0.5 ? -1 : 1
    var y = Math.random() * (max - min) + min
    var x = Math.random() * (max - min) + min
    this.moveY(y * diry)
    this.moveX(x * dirx)
  },
  moveY: function(byY) {
    var newY = this.y + byY
    if(0 > newY || newY < 1) {
      this.y += byY
    }
  },
  moveX: function(byX) {
    var newX = this.x + byX
    if(0 > newX || newX < 1) {
      this.x += byX
    }
  },
}

module.exports = Point

},{}],5:[function(require,module,exports){
var Predator = function(position) {
  this.position = position
}

Predator.prototype = {
  type: "Predator",
  move: function(close) {
    if(close.length > 0) {
      this.adjust(close)
    }
    else {
      this.wander()
    }
  },
  wander: function() {
    this.position.random()
  },
  adjust: function(close) {
    close.forEach(function(being) {
      var position = being.position
      if(being.type == "Predator") {
        this.position.towards(position, 0.1)
      }
      else if(being.type == "Prey") {
        this.position.towards(position, 0.25)
      }
    }.bind(this))
  },
}

module.exports = Predator

},{}],6:[function(require,module,exports){
var Prey = function(position) {
  this.position = position
}

Prey.prototype = {
  type: "Prey",
  move: function(close) {
    if(close.length > 0) {
      this.adjust(close)
    }
    else {
      this.wander()
    }
  },
  wander: function() {
    this.position.random()
  },
  adjust: function(close) {
    close.forEach(function(being) {
      var position = being.position
      if(being.type == "Predator") {
        this.position.away(position, 0.3)
      }
      else if(being.type == "Prey") {
        this.position.towards(position, 0.1)
      }
      else if(being.type == "Plant") {
        this.position.towards(position, 0.5)
      }
    }.bind(this))
  },
}

module.exports = Prey

},{}],7:[function(require,module,exports){
var Point    = require('./point')
var Plant    = require('./plant')
var Prey     = require('./prey')
var Predator = require('./predator')

var World = function() {
  this.beings = []
}

World.prototype = {
  numberOfPlants:     100,
  numberOfPrey:       750,
  numberOfPredators:  25,
  create: function() {
    this.generatePlants()
    this.generatePrey()
    this.generatePredators()
  },
  generatePlants: function() {
    var start = new Point(0.5, 0.55)
    this.generateBeings(start, this.numberOfPlants, Plant)
  },
  generatePrey: function() {
    var start = new Point(0.5, 0.5)
    this.generateBeings(start, this.numberOfPrey, Prey)
  },
  generatePredators: function() {
    var start = new Point(0.5, 0.45)
    this.generateBeings(start, this.numberOfPredators, Predator)
  },
  generateBeings: function(start, count, type) {
    for(var i = 0; i < count; i++) {
      this.generateBeing(start, type)
    }
  },
  generateBeing: function(start, type) {
    var randomStart = start.clone()
    randomStart.random(0.05, 0.005)
    var being = new type(randomStart)
    this.beings.push(being)
  },
  update: function() {
    this.beings.forEach(this.move.bind(this))
  },
  move: function(being) {
    var close = this.closeBeings(being)
    being.move(close)
  },
  closeBeings: function(currentBeing) {
    var maxDistance = 0.1
    var close = []
    this.beings.forEach(function(being) {
      if(being !== currentBeing) {
        var distance = currentBeing.position.distance(being.position)
        if(distance < maxDistance) { close.push(being) }
      }
    })
    return close;
  },
}

module.exports = World

},{"./plant":3,"./point":4,"./predator":5,"./prey":6}],8:[function(require,module,exports){
var Screen = function() {
  this.canvas = document.querySelector('#world')
  this.ctx    = this.canvas.getContext('2d')
  this.width  = this.canvas.width  = window.innerWidth
  this.height = this.canvas.height = window.innerHeight
}

Screen.prototype = {
  clear: function() {
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)
  },
  drawSquare: function(args) {
    var px = args.point.x * this.width
    var py = args.point.y * this.height
    this.ctx.fillStyle = args.colour
    this.ctx.fillRect(px, py, args.size, args.size)
  },
}

module.exports = Screen

},{}],9:[function(require,module,exports){
var Screen   = require('./screen')
var Plant    = require('../models/plant')
var Prey     = require('../models/prey')
var Predator = require('../models/predator')

var WorldView = function(world) {
  this.world  = world
  this.screen = new Screen()
}

WorldView.prototype = {
  cycle: function() {
    this.clear()
    this.update()
    this.draw()
    this.queue()
  },
  clear: function() {
    this.screen.clear()
  },
  update: function() {
    this.world.update()
  },
  draw: function() {
    this.drawBeings()
  },
  queue: function() {
    window.requestAnimationFrame(this.cycle.bind(this))
  },
  drawBeings: function() {
    this.world.beings.forEach(this.drawBeing.bind(this))
  },
  drawBeing: function(being) {
    this.screen.drawSquare({
      size   : 3,
      point  : being.position,
      colour : this.beingColour(being)
    })
  },
  beingColour: function(being) {
    if(being instanceof Plant) {
      return 'rgba(0, 255, 0, 0.9)'
    }
    else if(being instanceof Prey) {
      return 'rgba(255, 255, 0, 0.9)'
    }
    else if(being instanceof Predator) {
      return 'rgba(255, 0, 0, 0.9)'
    }
  },
}

module.exports = WorldView

},{"../models/plant":3,"../models/predator":5,"../models/prey":6,"./screen":8}]},{},[1]);
